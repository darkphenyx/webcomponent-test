import { TemplateRenderer } from './template-renderer.js';
import { sharedStyles } from './sharedStyles.js';

export class ProfileCard extends TemplateRenderer {
  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('click', () =>
      this.dispatchEvent(
        new CustomEvent('openDialog', { detail: this.profileData, bubbles: true, composed: true }),
      ),
    );
  }

  static get observedAttributes() {
    return ['profile-data'];
  }

  get template() {
    return `
      <style>
        :host {
            margin: 2px;
            display: block;
            padding: 30px;
            cursor: pointer;
            background: rgba(0,0,0,0.1);
            text-align: center;
            width: calc(25% - 4px);
            box-sizing: border-box;
            opacity: 0;
        }
        
        ${sharedStyles.card}
      </style>
      ${
        !!this.profileData
          ? `
        <img src=${this.profileData.picture.thumbnail} alt=${this.profileData.name.first}/>
        <h1>${this.capitalizeName(this.profileData.name.title)} 
            ${this.capitalizeName(this.profileData.name.first)} 
            ${this.capitalizeName(this.profileData.name.last)}
        </h1>
      `
          : ''
      }
     
    `;
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (newValue && name === 'profile-data') {
      this.profileData = JSON.parse(decodeURIComponent(newValue));
      this.removeAttribute('profile-data');
    }

    this.render();
  }

  capitalizeName(word) {
    return word[0].toUpperCase() + word.slice(1);
  }
}

customElements.define('profile-card', ProfileCard);
