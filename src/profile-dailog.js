import { TemplateRenderer } from './template-renderer.js';
import { sharedStyles } from './sharedStyles.js';

export class ProfileDialog extends TemplateRenderer {
  open(data) {
    this.profileData = data;
    this.render();
    this.shadowRoot.querySelector('dialog').showModal();
    this.addEventListener('click', () => this.shadowRoot.querySelector('dialog').close());
  }

  close() {
    this.shadowRoot.querySelector('dialog').close();
  }

  get template() {
    return `
      <style>
        dialog {
            max-width: 600px;
            width: auto;
            text-align: center;
        }
        
        dialog::backdrop {
        background: rgba(0,0,0,0.8);
        }
        
        div {
            display: flex;
            align-items: center;
            margin: 15px;
        }
        
        section {
            display: flex;
            justify-content: center;
        }
        
        .icon {
            width: 30px;
            height: 30px;
        }
        
        ${sharedStyles.card}
      </style>
      
      <dialog>
        ${
          !!this.profileData
            ? `
        <img src=${this.profileData.picture.thumbnail} alt=${this.profileData.name.first}/>
        <h1>${this.capitalizeName(this.profileData.name.title)} 
            ${this.capitalizeName(this.profileData.name.first)} 
            ${this.capitalizeName(this.profileData.name.last)}
        </h1>
        <section>
            <div>
                <img class="icon" src="https://www.materialui.co/materialIcons/hardware/smartphone_black_96x96.png" alt="">
                <p>${this.profileData.cell}</p>
            </div>
            
            <div>
                <img class="icon" src="https://www.materialui.co/materialIcons/communication/email_black_96x96.png" alt="">
                <p>${this.profileData.email}</p>
            </div>
        </section>
      `
            : ''
        }
        
        <p>Curabitur consectetur odio eu nulla facilisis convallis. Vivamus sapien elit, venenatis at purus eget, pellentesque dictum leo. Phasellus sed finibus eros. Proin mollis eros libero, eget imperdiet mauris varius non. Aliquam erat volutpat. Donec mattis nisi id magna porta, sed consectetur tellus dapibus. Morbi sagittis ultricies tincidunt. Nullam ac eleifend turpis. Sed viverra massa sed eros rhoncus, et aliquam orci varius. Fusce sit amet ultrices risus, ac ornare metus.</p>
         
      </dialog>
    `;
  }

  capitalizeName(word) {
    return word[0].toUpperCase() + word.slice(1);
  }
}

customElements.define('profile-dialog', ProfileDialog);
