import { TemplateRenderer } from './template-renderer.js';
import { sharedStyles } from './sharedStyles.js';
import './profile-card.js';
import './profile-cards-animator.js';
import './profile-dailog.js';

export class AppShell extends TemplateRenderer {
  constructor() {
    super();
    this.noShadow = true;
  }

  async getPeople() {
    const peoplePromise = await fetch('https://randomuser.me/api/?results=50');
    const res = await peoplePromise.json();
    this.people = res.results;
    this.render();
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('openDialog', evt =>
      this.querySelector('profile-dialog').open(evt.detail),
    );
    this.getPeople();
  }

  get template() {
    return `
        <style>
            :root {
                --primary: #4ca1b0;
            }
            
            app-shell {
                position: fixed;
                ${sharedStyles.trbl}
                overflow-y: overlay;
            }
        </style>
        
        <profile-dialog></profile-dialog>
        
        <profile-cards-animator>
        ${
          !!this.people
            ? this.people
                .map(
                  person =>
                    `<profile-card profile-data="${encodeURIComponent(
                      JSON.stringify(person),
                    )}"></profile-card>`,
                )
                .join('')
            : ''
        }
        </profile-cards-animator>
    `;
  }
}

customElements.define('app-shell', AppShell);
