#Web Component Test Ground

**You need an HTTP server in order for this to run**

- `npm i -g http-server`
- `cd webcomponent-test/src`
- `http-server -p 8000`

Now just go to 127.0.0.1:8000 and you can see the test ground.

